LATEXMK = latexmk
INKSCAPE = inkscape

TEXS = cv_fr.tex cv_en.tex
SVGS = images/envelope.svg	\
       images/phone.svg		\
       images/cloud.svg		\

TEX_PDFS = $(TEXS:%.tex=%.pdf)
SVG_PDFS = $(SVGS:%.svg=%.pdf)
PDFS = $(SVG_PDFS) $(TEX_PDFS)

all: $(PDFS)

images/%.pdf: images/%.svg
	$(INKSCAPE) -f $< -C -A $@

%.pdf: %.tex
	$(LATEXMK) -pdf $<

clean:
	$(RM) *.aux *.fdb_latexmk *.log *.out *.toc *.fls
	$(RM) $(TEX_PDFS)

distclean: clean
	$(RM) $(PDFS)
