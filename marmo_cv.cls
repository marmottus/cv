\NeedsTeXFormat{LaTeX2e}       % The version of LaTeX we are using
\ProvidesClass{marmo_cv}[2012/09/12 My custom CV class]

\LoadClass[10pt, a4paper]{article}
\usepackage[utf8x]{inputenc}
\pagestyle{empty}
\RequirePackage{titlesec}

%\usepackage{url}
\usepackage{vmargin}
\usepackage{color}
\usepackage{ulem}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{textpos}
\usepackage{fancybox}
\usepackage{palatino}
\usepackage{hyperref}

%% colors
\definecolor{darkblue}{rgb}{0.0,0.2,0.5}
\definecolor{darkblue2}{rgb}{0.0,0.1,0.3}
\definecolor{darkred}{rgb}{0.3,0.0,0.0}
\definecolor{darkgreen}{rgb}{0,0.3,0.0}

%% margins
\setmarginsrb{1cm}{5pt}{1cm}{4pt}{5pt}{6pt}{7pt}{8pt}

% Change section format
\titleformat{\section}
            {\bf\large\scshape\raggedright}
            {}{0em}
            {\textcolor{darkblue}}
            [\titlerule]

%%%%%%%%%%%%%%%%%%%%%%%
%% Default variables %%
%%%%%%%%%%%%%%%%%%%%%%%
\def\firstnamevar{First Name}
\def\lastnamevar{Last Name}

\def\addressvar{123 rue Foobar}
\def\addresszipvar{424242}
\def\addresstownvar{Nowhere}

\def\datesvar{January -- December 3012}
\def\photovar{images/photo.jpg}

\def\emailvar{foo@bar.com}
\def\phonevar{(+33) 142.424.242}
\def\websitevar{http://exemple.com}
\def\agevar{42 ans}
\def\functionvar{Développeur C++}

%%%%%%%%%%%%%%%%%%%%%%%
%% Variable commands %%
%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\name}[2]{
  \def\firstnamevar{#1}
  \def\lastnamevar{#2}
}

\newcommand{\dates}[1]{
  \def\datesvar{#1}
}

\newcommand{\function}[1]{
  \def\functionvar{#1}
}

\newcommand{\email}[1]{
  \def\emailvar{#1}
}

\newcommand{\phone}[1]{
  \def\phonevar{#1}
}

\newcommand{\website}[1]{
  \def\websitevar{#1}
}

\newcommand{\age}[1]{
  \def\agevar{#1}
}

\newcommand{\address}[3]{
  \def\addressvar{#1}
  \def\addresszipvar{#2}
  \def\addresstownvar{#3}
}

\newcommand{\photo}[1]{
  \def\photovar{#1}
}

%%%%%%%%%%%%%%%%%%%%
%% Style commands %%
%%%%%%%%%%%%%%%%%%%%
\newcommand{\link}[1]{
  \texttt{\bf \textcolor{darkblue}{\uline{#1}}}
}

\newcommand{\cvsection}[1]{
  \section{#1}
}

\newcommand{\Keyword}[1]{
  \textcolor{darkred}{\bf \uline{#1}}
}

\newcommand{\keyword}[1]{
  {\bf \textcolor{darkblue2}{#1}}
}

%%%%%%%%%%%%%%%
%% CV Header %%
%%%%%%%%%%%%%%%
\newcommand{\header}{

  \setlength{\unitlength}{1mm}

  \thicklines
  \begin{picture}(0,0)
    \put(91,-17){\oval(200,50)}
  \end{picture}

  %% Photo
  \begin{textblock}{1}(12,-0.3)
    \fbox{\includegraphics[height=3cm]{\photovar}}
  \end{textblock}

  \hspace{-0.5cm}
  %% Name
  \begin{tabular}{c}
    {\Large \firstnamevar~\textsc{\lastnamevar}}        \\
    {\large \addressvar}                                \\
    {\large \addresszipvar~\textsc{\addresstownvar}}    \\
    \rule[2pt]{24pt}{1pt}                               \\
  \end{tabular}

  \hspace{-1cm}
  \begin{tabular}{p{4cm}m{10.6cm}m{4.6cm}}

    %% Contact
    \begin{tabular}{p{0.5cm}p{4cm}}
        \includegraphics[width=12pt]{images/envelope}   &
        \link{\emailvar}                                \\

        \includegraphics[width=12pt]{images/phone}      &
        \phonevar                                       \\

        \includegraphics[width=12pt]{images/cloud}      &
        \link{\websitevar}                              \\
    \end{tabular}                                               &

    %% Function and age
    \begin{center}
      {\large \keyword{\textsc{\functionvar}}}          \\
      {\normalsize \datesvar}
    \end{center}                                                &

    \begin{center}
      \vspace{0.8cm} \agevar                                        \\
    \end{center}
  \end{tabular}\\
  \vspace{0.3cm}
}


%%%%%%%%%%%%%%%
%% Education %%
%%%%%%%%%%%%%%%
\newenvironment{education}{
  \renewcommand\item[3]{
    \hspace{-0.5cm}
    ##1 & ##2 & ##3 \\
    }

  \begin{tabular}{p{1.7cm}p{10.2cm}p{5cm}}
}{
  \end{tabular}
}

%%%%%%%%%%%%%%%
%% Languages %%
%%%%%%%%%%%%%%%
\newenvironment{languages}{
  \renewcommand\item[2]{
    \keyword{##1} & ##2\\
  }
  \begin{tabular}{p{3cm}l}
}{
  \end{tabular}
}

%%%%%%%%%%%%%%%%
%% Experience %%
%%%%%%%%%%%%%%%%
\newenvironment{experience}{
  \renewcommand\title[2]{
    \begin{tabular}{p{5cm}l}
      ##1 & \textsc{\textcolor{darkred}{\uline{##2}}}\\
    \end{tabular}\\
  }

  \newenvironment{items}{
    \let\olditem\item
    \renewcommand\item[1]{
        \olditem ####1
    }
    \begin{itemize}
  }{
    \end{itemize}
  }
}{
}
